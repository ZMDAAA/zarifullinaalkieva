﻿namespace редактор_векторной_графики
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выйтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обАвторахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Кисть = new System.Windows.Forms.PictureBox();
            this.Ластик = new System.Windows.Forms.PictureBox();
            this.Линии = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Карандаш = new System.Windows.Forms.PictureBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.Фигуры = new System.Windows.Forms.PictureBox();
            this.Текст = new System.Windows.Forms.PictureBox();
            this.Пипетка = new System.Windows.Forms.PictureBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Кисть)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ластик)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Линии)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Карандаш)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Фигуры)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Текст)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Пипетка)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(787, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.выйтиToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // выйтиToolStripMenuItem
            // 
            this.выйтиToolStripMenuItem.Name = "выйтиToolStripMenuItem";
            this.выйтиToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.выйтиToolStripMenuItem.Text = "Выйти";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обАвторахToolStripMenuItem});
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            // 
            // обАвторахToolStripMenuItem
            // 
            this.обАвторахToolStripMenuItem.Name = "обАвторахToolStripMenuItem";
            this.обАвторахToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.обАвторахToolStripMenuItem.Text = "Об авторах";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Location = new System.Drawing.Point(103, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(684, 384);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.Кисть);
            this.panel1.Controls.Add(this.Ластик);
            this.panel1.Controls.Add(this.Линии);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Карандаш);
            this.panel1.Controls.Add(this.trackBar1);
            this.panel1.Controls.Add(this.Фигуры);
            this.panel1.Controls.Add(this.Текст);
            this.panel1.Controls.Add(this.Пипетка);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(97, 384);
            this.panel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 349);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 26);
            this.label2.TabIndex = 24;
            this.label2.Text = "Сейчас:\r\nинструмент\r\n";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 319);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Цвет";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Кисть
            // 
            this.Кисть.BackColor = System.Drawing.Color.Transparent;
            this.Кисть.Cursor = System.Windows.Forms.Cursors.Default;
            this.Кисть.Image = global::редактор_векторной_графики.Properties.Resources.brush;
            this.Кисть.Location = new System.Drawing.Point(15, 73);
            this.Кисть.Name = "Кисть";
            this.Кисть.Size = new System.Drawing.Size(22, 22);
            this.Кисть.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Кисть.TabIndex = 22;
            this.Кисть.TabStop = false;
            this.Кисть.Tag = "5";
            this.Кисть.Click += new System.EventHandler(this.objectClick);
            // 
            // Ластик
            // 
            this.Ластик.BackColor = System.Drawing.Color.Transparent;
            this.Ластик.Cursor = System.Windows.Forms.Cursors.Default;
            this.Ластик.Image = global::редактор_векторной_графики.Properties.Resources.eraser;
            this.Ластик.Location = new System.Drawing.Point(16, 17);
            this.Ластик.Name = "Ластик";
            this.Ластик.Size = new System.Drawing.Size(22, 22);
            this.Ластик.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Ластик.TabIndex = 13;
            this.Ластик.TabStop = false;
            this.Ластик.Tag = "1";
            this.Ластик.Click += new System.EventHandler(this.objectClick);
            // 
            // Линии
            // 
            this.Линии.BackColor = System.Drawing.Color.Transparent;
            this.Линии.Cursor = System.Windows.Forms.Cursors.Default;
            this.Линии.Image = global::редактор_векторной_графики.Properties.Resources.line_curve;
            this.Линии.Location = new System.Drawing.Point(48, 17);
            this.Линии.Name = "Линии";
            this.Линии.Size = new System.Drawing.Size(22, 22);
            this.Линии.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Линии.TabIndex = 14;
            this.Линии.TabStop = false;
            this.Линии.Tag = "2";
            this.Линии.Click += new System.EventHandler(this.objectClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 252);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Ширина:";
            // 
            // Карандаш
            // 
            this.Карандаш.BackColor = System.Drawing.Color.Transparent;
            this.Карандаш.Cursor = System.Windows.Forms.Cursors.Default;
            this.Карандаш.Image = global::редактор_векторной_графики.Properties.Resources.pencil;
            this.Карандаш.Location = new System.Drawing.Point(15, 45);
            this.Карандаш.Name = "Карандаш";
            this.Карандаш.Size = new System.Drawing.Size(22, 22);
            this.Карандаш.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Карандаш.TabIndex = 15;
            this.Карандаш.TabStop = false;
            this.Карандаш.Tag = "3";
            this.Карандаш.Click += new System.EventHandler(this.objectClick);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(6, 268);
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(87, 45);
            this.trackBar1.TabIndex = 19;
            this.trackBar1.Value = 1;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // Фигуры
            // 
            this.Фигуры.BackColor = System.Drawing.Color.Transparent;
            this.Фигуры.Cursor = System.Windows.Forms.Cursors.Default;
            this.Фигуры.Image = global::редактор_векторной_графики.Properties.Resources.figures;
            this.Фигуры.Location = new System.Drawing.Point(20, 101);
            this.Фигуры.Name = "Фигуры";
            this.Фигуры.Size = new System.Drawing.Size(45, 23);
            this.Фигуры.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Фигуры.TabIndex = 16;
            this.Фигуры.TabStop = false;
            this.Фигуры.Tag = "7";
            this.Фигуры.Click += new System.EventHandler(this.objectClick);
            // 
            // Текст
            // 
            this.Текст.BackColor = System.Drawing.Color.Transparent;
            this.Текст.Cursor = System.Windows.Forms.Cursors.Default;
            this.Текст.Image = global::редактор_векторной_графики.Properties.Resources.text;
            this.Текст.Location = new System.Drawing.Point(48, 45);
            this.Текст.Name = "Текст";
            this.Текст.Size = new System.Drawing.Size(22, 22);
            this.Текст.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Текст.TabIndex = 18;
            this.Текст.TabStop = false;
            this.Текст.Tag = "4";
            this.Текст.Click += new System.EventHandler(this.objectClick);
            // 
            // Пипетка
            // 
            this.Пипетка.BackColor = System.Drawing.Color.Transparent;
            this.Пипетка.Cursor = System.Windows.Forms.Cursors.Default;
            this.Пипетка.Image = global::редактор_векторной_графики.Properties.Resources.pipette;
            this.Пипетка.Location = new System.Drawing.Point(48, 73);
            this.Пипетка.Name = "Пипетка";
            this.Пипетка.Size = new System.Drawing.Size(22, 22);
            this.Пипетка.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.Пипетка.TabIndex = 17;
            this.Пипетка.TabStop = false;
            this.Пипетка.Tag = "6";
            this.Пипетка.Click += new System.EventHandler(this.objectClick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "jpg";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Квадрат",
            "Круг",
            "Треугольник"});
            this.comboBox1.Location = new System.Drawing.Point(6, 131);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(70, 21);
            this.comboBox1.TabIndex = 25;
            this.comboBox1.Text = "Фигура:";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 408);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Кисть)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ластик)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Линии)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Карандаш)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Фигуры)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Текст)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Пипетка)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выйтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem обАвторахToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox Кисть;
        private System.Windows.Forms.PictureBox Ластик;
        private System.Windows.Forms.PictureBox Линии;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox Карандаш;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.PictureBox Фигуры;
        private System.Windows.Forms.PictureBox Текст;
        private System.Windows.Forms.PictureBox Пипетка;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

