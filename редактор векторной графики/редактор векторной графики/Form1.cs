﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace редактор_векторной_графики
{
    public partial class Form1 : Form
    {
        Bitmap pic;
        public int x1, y1, c1;

        Pen pen; Color c;

        PictureBox[] buttons;
        public int instrument;

        public Form1()
        {
            InitializeComponent();
            pic = new Bitmap(1000, 1000);
            x1 = y1 = 0;

            buttons = new PictureBox[]
            {
                Ластик, Линии, Карандаш, Фигуры, Текст, Кисть
            };
            
        }

        void objectClick(object sender, EventArgs e)
        {
            var button = sender as PictureBox;
            instrument = Convert.ToInt32(button.Tag);

            label2.Text = "Сейчас: \n" + button.Name;
        }

        private void button1_Click(object sender, EventArgs e)//установка цвета
        {
            ColorDialog cd = new ColorDialog();
            cd.AllowFullOpen = false;
            cd.ShowHelp = true;
            cd.Color = c;
            if (cd.ShowDialog() == DialogResult.OK)
                c = cd.Color;
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            label1.Text = "Ширина: " + trackBar1.Value;
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName !="")
            {
                pic.Save(saveFileDialog1.FileName);
            }
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName != "")
            {
                pic = (Bitmap)Image.FromFile(openFileDialog1.FileName);
                pictureBox1.Image = pic;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            c = Color.Black; pen = new Pen(c, 1);
            label2.Text = "Сейчас:\n Карандаш";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            switch (instrument)
            {
                case 1: //ластик +
                    c = Color.White;
                    pen = new Pen(c, trackBar1.Value);
                    break;
                case 2: //линии

                    break;
                case 3: //карандаш
                    int x = 1;
                    pen = new Pen(c, x);
                    break;
                case 4: //текст

                    break;
                case 5: //кисть
                    pen = new Pen(c, trackBar1.Value);

                    pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
                    pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;

                    break;
                case 6: //пипетка

                    break;
                case 7: //фигуры

                    break;
                default:
                    break;

            }

            Graphics g = Graphics.FromImage(pic);

            if (e.Button == MouseButtons.Left)
            {
                g.DrawLine(pen, x1, y1, e.X, e.Y);
                pictureBox1.Image = pic;
            }

            x1 = e.X; y1 = e.Y;
        }
    }
}
